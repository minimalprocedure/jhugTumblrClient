/*
 * JavaScript file
 */

var APPLICATION_VERSION = {
		'name' : 'Jhug Tumblr Client',
		'major' : 0,
		'minor' : 6,
		'subminor' : 6,
		'patch' : 0,
		'betaFlag' : 'beta',
		versionString : function versionString() {
			return this['major'].toString() + '.' + this['minor'].toString() + '.'
					+ this['subminor'].toString() + '.' + this['patch'].toString()
					+ this['betaFlag'];
		}
	};

NOKIA_PATH_JAVASCRIPT = 'lib/';
NOKIA_PATH_STYLE_ROOT = 'themes/nokia/base/';

var APPLICATION = undefined;
var EMULATOR = undefined;

if (window.widget) {
	widget.setNavigationEnabled(true);
	APPLICATION = window.widget;
	if ($.browser.mozilla)
		EMULATOR = true;
	else
		EMULATOR = false;
} else {
	EMULATOR = true;
	APPLICATION = {
		__data__ : {},
		setPreferenceForKey : function(value, key) {
			this.__data__[key] = value;
		},
		preferenceForKey : function(key) {
			return this.__data__[key];
		}
	};
}

function toggleByTwo(el1, el2) {
	$('#' + el1).toggleClass('Closed');
	$('#' + el2).toggleClass('Closed');
	return false;
}

function fileOrSource(el, block) {
	$.string(String.prototype);
	var typeInfo = $('#' + el).parent('form').find('div.AreaWriteTypeField');
	var field = 'source';
	if (typeInfo) {
		if ($(typeInfo).text() == 'photo') {
			field = 'source';
		} else if ($(typeInfo).text() == 'video') {
			field = 'embed';
		} else if ($(typeInfo).text() == 'audio') {
			field = 'externally-hosted-url';
		} else {
			field = 'source';
		}
	}
	var file =
			'<label class="Inline" for="data">File:</label>'
					+ '<a class="toggleFileSource" href="#" onclick="fileOrSource(\''
					+ el + '\',\'' + field + '\'); return false;">use source</a>'
					+ '<input type="file" id="AreaWrite' + field.capitalize()
					+ '-data" name="data" class="ui-state-default" />';
	var source =
			'<label class="Inline" for="source">Source:</label>'
					+ '<a class="toggleFileSource" href="#" onclick="fileOrSource(\''
					+ el + '\',\'file\'); return false;">use file</a>'
					+ '<textarea id="AreaWrite' + field.capitalize()
					+ '-source" name="source" class="ui-state-default small"></textarea>';
	var embed =
			'<label class="Inline" for="embed">Embed:</label>'
					+ '<a class="toggleFileSource" href="#" onclick="fileOrSource(\''
					+ el + '\',\'file\'); return false;">use file</a>'
					+ '<textarea id="AreaWrite' + field.capitalize()
					+ '-embed" name="embed" class="ui-state-default small"></textarea>';
	
	var externally =
			'<label class="Inline" for="externally-hosted-url">Externally hosted url:</label>'
					+ '<a class="toggleFileSource" href="#" onclick="fileOrSource(\''
					+ el
					+ '\',\'file\'); return false;">use file</a>'
					+ '<textarea id="AreaWrite'
					+ field.capitalize()
					+ '-externally-hosted-url" name="externally-hosted-url" class="ui-state-default small"></textarea>';
	
	if (block == 'file') {
		$('#' + el).html(file);
	} else if (block == 'source') {
		$('#' + el).html(source);
	} else if (block == 'embed') {
		$('#' + el).html(embed);
	} else {
		$('#' + el).html(externally);
	}
	return false;
}

function initMenu() {
	
	var mainMenu = new Nokia.OptionsMenu( {
		element : '#MenuMain',
		transitions : false
	});
	
	var menuItemPosts = new Nokia.OptionsMenuSection( {
		label : 'Posts',
		select : function() {
			showArea('AreaPosts');
		}
	
	});
	
	var menuItemPostsAll = new Nokia.OptionsMenuItem( {
		label : 'All',
		select : function() {
			showArea('AreaPosts');
			showToolBar('ToolBarPosts');
			if (getTumblrSettings()) {
				splashShow(TumblrApi);
				TumblrApi.getPosts('AreaPosts', true);
			}
		}
	});
	
	var menuItemPostsLike = new Nokia.OptionsMenuItem( {
		label : 'Likes',
		select : function() {
			showArea('AreaPosts');
			showToolBar('ToolBarPosts');
			if (getTumblrSettings()) {
				splashShow(TumblrApi);
				TumblrApi.getPosts('AreaPosts', true, 'likes');
			}
		}
	});
	
	menuItemPosts.addItem(menuItemPostsAll);
	menuItemPosts.addItem(menuItemPostsLike);
	
	var menuItemNew = new Nokia.OptionsMenuSection( {
		label : 'New'
	});
	
	var menuItemNewPhoto = new Nokia.OptionsMenuItem( {
		label : 'Photo',
		select : function() {
			showArea('AreaWritePhoto');
			showToolBar('ToolBarNew');
			$('#AreaWritePhoto-tags').val(TumblrApi.tags);
		}
	});
	
	var menuItemNewQuote = new Nokia.OptionsMenuItem( {
		label : 'Quote',
		select : function() {
			showArea('AreaWriteQuote');
			showToolBar('ToolBarNew');
			$('#AreaWriteQuote-tags').val(TumblrApi.tags);
		}
	});
	
	var menuItemNewLink = new Nokia.OptionsMenuItem( {
		label : 'Link',
		select : function() {
			showArea('AreaWriteLink');
			showToolBar('ToolBarNew');
			$('#AreaWriteLink-tags').val(TumblrApi.tags);
		}
	});
	
	var menuItemNewChat = new Nokia.OptionsMenuItem( {
		label : 'Chat',
		select : function() {
			showArea('AreaWriteChat');
			showToolBar('ToolBarNew');
			$('#AreaWriteChat-tags').val(TumblrApi.tags);
		}
	});
	
	var menuItemNewText = new Nokia.OptionsMenuItem( {
		label : 'Text',
		select : function() {
			showArea('AreaWriteText');
			showToolBar('ToolBarNew');
			$('#AreaWriteText-tags').val(TumblrApi.tags);
		}
	});
	
	var menuItemNewAudio = new Nokia.OptionsMenuItem( {
		label : 'Audio',
		select : function() {
			showArea('AreaWriteAudio');
			showToolBar('ToolBarNew');
			$('#AreaWriteAudio-tags').val(TumblrApi.tags);
		}
	});
	
	var menuItemNewVideo = new Nokia.OptionsMenuItem( {
		label : 'Video',
		select : function() {
			showArea('AreaWriteVideo');
			showToolBar('ToolBarNew');
			$('#AreaWriteVideo-tags').val(TumblrApi.tags);
		}
	});
	
	menuItemNew.addItem(menuItemNewText);
	menuItemNew.addItem(menuItemNewPhoto);
	menuItemNew.addItem(menuItemNewQuote);
	menuItemNew.addItem(menuItemNewLink);
	menuItemNew.addItem(menuItemNewChat);
	menuItemNew.addItem(menuItemNewAudio);
	menuItemNew.addItem(menuItemNewVideo);
	
	var menuItemAbout = new Nokia.OptionsMenuSection( {
		label : 'About'
	});
	
	var menuItemAboutInfo = new Nokia.OptionsMenuItem( {
		label : 'Informations',
		select : function() {
			showToolBar('ToolBarInformation');
			showArea('AreaConfiguration');
			showArea('AreaAboutInfo');
		}
	});
	
	var menuItemAboutConfiguration =
			new Nokia.OptionsMenuItem( {
				label : 'Options',
				select : function() {
					showToolBar('ToolBarConfiguration');
					showArea('AreaConfiguration');
					$('#AreaConfiguration-email').val(
							APPLICATION.preferenceForKey('email'));
					$('#AreaConfiguration-password').val(
							APPLICATION.preferenceForKey('password'));
					$('#AreaConfiguration-defaultsTitle').val(
							APPLICATION.preferenceForKey('defaultsTitle'));
					$('#AreaConfiguration-tags')
							.val(APPLICATION.preferenceForKey('tags'));
					$('#AreaConfiguration-signature').val(
							APPLICATION.preferenceForKey('signature'));
					$('#AreaConfiguration-licence').val(
							APPLICATION.preferenceForKey('licence'));
					$('#AreaConfiguration-hideMediaViewer').val(
							APPLICATION.preferenceForKey('hideMediaViewer'));
				}
			});
	
	menuItemAbout.addItem(menuItemAboutInfo);
	menuItemAbout.addItem(menuItemAboutConfiguration);
	
	mainMenu.addSection(menuItemPosts);
	mainMenu.addSection(menuItemNew);
	mainMenu.addSection(menuItemAbout);
	mainMenu.create();
	
	// Nokia.hideSplash();
}

function initToolbarPosts() {
	var back = new Nokia.Button( {
		element : '#PostsBack',
		disabled : false,
		keeppressed : false,
		label : 'backward',
		click : function() {
			if (getTumblrSettings()) {
				splashShow(TumblrApi);
				TumblrApi.getPosts('AreaPosts', 'backward');
			}
		}
	});
	
	var fore = new Nokia.Button( {
		element : '#PostsFore',
		disabled : false,
		keeppressed : false,
		label : 'forward',
		click : function() {
			if (getTumblrSettings()) {
				splashShow(TumblrApi);
				TumblrApi.getPosts('AreaPosts', 'forward');
			}
		}
	});
	
	var all = new Nokia.Button( {
		element : '#PostsAll',
		disabled : false,
		keeppressed : false,
		label : 'reload',
		click : function() {
			if (getTumblrSettings()) {
				splashShow(TumblrApi);
				TumblrApi.getPosts('AreaPosts', true);
			}
		}
	});
	
	var likes = new Nokia.Button( {
		element : '#PostsLiked',
		disabled : false,
		keeppressed : false,
		label : 'likes',
		click : function() {
			if (getTumblrSettings()) {
				splashShow(TumblrApi);
				TumblrApi.getPosts('AreaPosts', true, 'likes');
			}
		}
	});
	
}

function showToolBar(toolbarID) {
	var children = $('#ToolBars').children();
	$.each(children, function() {
		if ($(this).attr('id') != toolbarID) {
			$(this).hide();
		} else {
			$(this).show();
		}
	});
}

function showArea(areaID) {
	var children = $('#AreaMain').children();
	$.each(children, function() {
		if ($(this).attr('id') != areaID) {
			$(this).hide();
		} else {
			$(this).show();
		}
	});
}

function initToolBarNew() {
	
	var submitDefaults = new Nokia.CheckBox( {
		element : '#ToolBarNew-submitDefaults',
		label : 'defaults',
		wrapper : 'span',
		checked : true,
		create : function(event) {
			TumblrApiSettings.submitDefaults = true;
		},
		check : function(event) {
			TumblrApiSettings.submitDefaults = true;
		},
		uncheck : function(event) {
			TumblrApiSettings.submitDefaults = false;
		}
	});
}

function initGui() {
	
	showToolBar('none');
	showArea('none');
	
	var hideMediaViewer = new Nokia.CheckBox( {
		element : '#AreaConfiguration-hideMediaViewer',
		wrapper : 'span',
		checked : APPLICATION.preferenceForKey('hideMediaViewer'),
		check : function(event) {
			$('#AreaConfiguration-hideMediaViewer').val(true);
		},
		uncheck : function(event) {
			$('#AreaConfiguration-hideMediaViewer').val(false);
		}
	});
	
	var areaWriteTextSubmit = new Nokia.Button( {
		element : '#AreaWriteText-submit',
		disabled : false,
		keeppressed : false,
		label : 'write',
		click : function() {
			if (getTumblrSettings()) {
				splashShowForWrite(TumblrApi);
				attachDefaults(TumblrApi);
				TumblrApi.write('regular', 'AreaWriteText-data');
			}
		}
	});
	
	var areaWriteQuoteSubmit = new Nokia.Button( {
		element : '#AreaWriteQuote-submit',
		disabled : false,
		keeppressed : false,
		label : 'write',
		click : function() {
			if (getTumblrSettings()) {
				splashShowForWrite(TumblrApi);
				attachDefaults(TumblrApi);
				TumblrApi.write('quote', 'AreaWriteQuote-data');
			}
		}
	});
	
	var areaWriteLinkSubmit = new Nokia.Button( {
		element : '#AreaWriteLink-submit',
		disabled : false,
		keeppressed : false,
		label : 'write',
		click : function() {
			if (getTumblrSettings()) {
				splashShowForWrite(TumblrApi);
				attachDefaults(TumblrApi);
				TumblrApi.write('link', 'AreaWriteLink-data');
			}
		}
	});
	
	var areaWriteChatSubmit = new Nokia.Button( {
		element : '#AreaWriteChat-submit',
		disabled : false,
		keeppressed : false,
		label : 'write',
		click : function() {
			if (getTumblrSettings()) {
				splashShowForWrite(TumblrApi);
				attachDefaults(TumblrApi);
				TumblrApi.write('conversation', 'AreaWriteChat-data');
			}
		}
	});
	
	var areaWritePhotoSubmit = new Nokia.Button( {
		element : '#AreaWritePhoto-submit',
		disabled : false,
		keeppressed : false,
		label : 'write',
		click : function() {
			if (getTumblrSettings()) {
				splashShowForWrite(TumblrApi);
				attachDefaults(TumblrApi);
				TumblrApi.write('photo', 'AreaWritePhoto-data', 'multipart');
			}
		}
	});
	
	var areaWriteAudioSubmit = new Nokia.Button( {
		element : '#AreaWriteAudio-submit',
		disabled : false,
		keeppressed : false,
		label : 'write',
		click : function() {
			if (getTumblrSettings()) {
				splashShowForWrite(TumblrApi);
				attachDefaults(TumblrApi);
				TumblrApi.write('audio', 'AreaWriteAudio-data', 'multipart');
			}
		}
	});
	
	var areaWriteVideoSubmit = new Nokia.Button( {
		element : '#AreaWriteVideo-submit',
		disabled : false,
		keeppressed : false,
		label : 'write',
		click : function() {
			if (getTumblrSettings()) {
				splashShowForWrite(TumblrApi);
				attachDefaults(TumblrApi);
				TumblrApi.write('video', 'AreaWriteVideo-data', 'multipart');
			}
		}
	});
}

function loadConfiguration() {
	var email = $('#AreaConfiguration-email').val();
	var password = $('#AreaConfiguration-password').val();
	var tags = $('#AreaConfiguration-tags').val();
	
	var defaultsTitle = $('#AreaConfiguration-defaultsTitle').val();
	var signature = $('#AreaConfiguration-signature').val();
	var licence = $('#AreaConfiguration-licence').val();
	
	var hideMediaViewer = $('#AreaConfiguration-hideMediaViewer').val();
	
	APPLICATION.setPreferenceForKey('justRunned', 'firstRun');
	APPLICATION.setPreferenceForKey(email, 'email');
	APPLICATION.setPreferenceForKey(password, 'password');
	APPLICATION.setPreferenceForKey(tags, 'tags');
	APPLICATION.setPreferenceForKey(defaultsTitle, 'defaultsTitle');
	APPLICATION.setPreferenceForKey(signature, 'signature');
	APPLICATION.setPreferenceForKey(licence, 'licence');
	
	APPLICATION.setPreferenceForKey(hideMediaViewer, 'hideMediaViewer');
	
	TumblrApiSettings.email = email;
	TumblrApiSettings.password = password;
	TumblrApiSettings.tags = tags;
	TumblrApiSettings.defaultsTitle = defaultsTitle;
	TumblrApiSettings.signature = signature;
	TumblrApiSettings.licence = licence;
	
	TumblrApiSettings.hideMediaViewer = hideMediaViewer;
}

function initAreaConfiguration() {
	
	var AreaConfigurationSubmit = new Nokia.Button( {
		element : '#AreaConfiguration-submit',
		disabled : false,
		keeppressed : false,
		label : 'save',
		click : function() {			
			loadConfiguration();			
			$('#AreaConfiguration').hide();
			showToolBar('none');
		}
	});
}

function attachDefaults(obj, title) {
	$.string(String.prototype);
	if (!title) {
		title = TumblrApiSettings.defaultsTitle;
		if (title == undefined || title.blank != true) {
			title = '<dt class="DefaultsTitle"><strong>' + title + '</strong></dt>';
		} else {
			title = undefined;
		}
	} else {
		title = '<dt class="DefaultsTitle"><strong>' + title + '</strong></dt>';
	}
	
	var signature = TumblrApiSettings.signature;
	if (signature != undefined || signature.blank != true) {
		signature =
				'<dd class="DefaultsSignature">' + TumblrApiSettings.signature
						+ '</dd>';
	} else {
		signature = undefined;
	}
	
	var licence = TumblrApiSettings.licence;
	if (licence != undefined || licence.blank != true) {
		licence =
				'<dd class="DefaultsLicence"><strong>licence: </strong>'
						+ TumblrApiSettings.licence + '</dd>';
	} else {
		licence = undefined;
	}
	
	if (signature || licence) {
		var defaults = '<p><dl class="Defaults">';
		
		if (title)
			defaults += title;
		if (signature)
			defaults += signature;
		if (licence)
			defaults += licence;
		
		defaults += '</dl></p>';
		
		obj.defaultsAttachs = defaults;
	} else {
		obj.defaultsAttachs = undefined;
	}
	
	var tags = TumblrApiSettings.tags;
	if (tags == undefined || tags.blank == true) {
		tags = '';
	}
	
	obj.defaultsTags = tags;
	return defaults;
}

function getTumblrSettings() {
	
	if (TumblrApiSettings.email && TumblrApiSettings.password)
		return true;
	
	if (APPLICATION.preferenceForKey('firstRun') == 'justRunned') {
		
		TumblrApiSettings.email = APPLICATION.preferenceForKey('email');
		TumblrApiSettings.password = APPLICATION.preferenceForKey('password');
		TumblrApiSettings.tags = APPLICATION.preferenceForKey('tags');
		TumblrApiSettings.defaultsTitle =
				APPLICATION.preferenceForKey('defaultsTitle');
		TumblrApiSettings.signature = APPLICATION.preferenceForKey('signature');
		TumblrApiSettings.licence = APPLICATION.preferenceForKey('licence');
		
		var hide = APPLICATION.preferenceForKey('hideMediaViewer');
		if (hide == 'true') {
			TumblrApiSettings.hideMediaViewer = true;
		} else {
			TumblrApiSettings.hideMediaViewer = false;
		}
		
		if (!TumblrApiSettings.email || !TumblrApiSettings.password) {
			showToolBar('ToolBarConfiguration');
			$('#AreaConfiguration').show();
			return false;
		} else
			return true;
	} else {
		showToolBar('ToolBarConfiguration');
		$('#AreaConfiguration').show();
	}
	return true;
}

// function firstRun() {
// showToolBar('ToolBarConfiguration');
// alert(app.preferenceForKey('firstRun'));
// if (!APPLICATION.preferenceForKey('firstRun')) {
// $('#AreaConfiguration').show();
// }
// }

function splashShow(obj) {
	//Nokia.hideSplash();
	//$('#LoaderIndicator').hide();
	obj.currentCallback = function() {
		$('#LoaderIndicator').hide();
		//Nokia.hideSplash();
	};
	//Nokia.showSplash('<img src="resources/images/spinner.gif" />');
	$('#LoaderIndicator').show();
}

function splashShowForWrite(obj) {
	//Nokia.hideSplash();
	//$('#LoaderIndicator').hide();
	obj.currentCallback = function() {
		$('#LoaderIndicator').hide();
		//Nokia.hideSplash();
	};
	//Nokia.showSplash('<img src="resources/images/spinner.gif" />');
	$('#LoaderIndicator').show();
}

function init() {
	initAreaConfiguration();
	initMenu();
	initToolbarPosts();
	initToolBarNew();
	initGui();
	getTumblrSettings();
}

$(window).resize(function() {
	detectResolution();
	// resolution = RESOLUTION_NHD_LANDSCAPE;
	switch (resolution) {
		case RESOLUTION_NHD_PORTRAIT: {
			$('#AreaMain').removeClass('AreaMainLandscape');
			$('#AreaMain').addClass('AreaMainPortrait');
			break;
		}
		case RESOLUTION_NHD_LANDSCAPE: {
			$('#AreaMain').removeClass('AreaMainPortrait');
			$('#AreaMain').addClass('AreaMainLandscape');
			break;
		}
		default:
			break;
	}
});

$(document).ready(function() {
	$('#AreaMain').toggleClass('AreaMainPortrait');
	$(window).trigger('resize');
	Nokia.use('optionsmenu', 'scroll', 'button', 'checkbox', init);
	$('#ApplicationNameBlock').text(APPLICATION_VERSION['name']);
	$('#ApplicationVersionBlock').text(APPLICATION_VERSION.versionString());
});
