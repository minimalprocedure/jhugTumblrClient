// Device resolutions. Can be used for device-specific customization.
var RESOLUTION_UNDEFINED = 0;
var RESOLUTION_QVGA_LANDSCAPE = 1;  // 320x240
var RESOLUTION_QVGA_PORTRAIT = 2;   // 240x320
var RESOLUTION_NHD_LANDSCAPE = 3;   // 640x360
var RESOLUTION_NHD_PORTRAIT = 4;    // 360x640
var RESOLUTION_HOME_SCREEN = 5;     // less than 75 % of the resolutions above
var resolution = RESOLUTION_UNDEFINED;


 
// Detects the resolution of the device
function detectResolution() {	
    var screenWidth = screen.width;
    var screenHeight = screen.height;
    var windowWidth = window.innerWidth;
    var windowHeight = window.innerHeight;
 
    if (windowHeight < (0.75 * screenHeight) ||
        windowWidth < (0.75 * screenWidth)) {
        // If the window width or height is less than 75 % of the screen width
        // or height, we assume the home screen view should be active
        resolution = RESOLUTION_HOME_SCREEN;
    } else if (screenWidth == 240 && screenHeight == 320) {
        resolution = RESOLUTION_QVGA_PORTRAIT;
    } else if (screenWidth == 320 && screenHeight == 240) {
        resolution = RESOLUTION_QVGA_LANDSCAPE;
    } else if (screenWidth == 360 && screenHeight == 640) {
        resolution = RESOLUTION_NHD_PORTRAIT;
    } else if (screenWidth == 640 && screenHeight == 360) {
        resolution = RESOLUTION_NHD_LANDSCAPE;
    } else {
        resolution = RESOLUTION_UNDEFINED;
    }
    return resolution;
}

