/*!
 * jhugApi Tumblr Client
 * version: 1.0.0.0beta (04-OCT-2010)
 * @requires jQuery v1.3.2 or later (http://jquery.com/)
 * @requires jQuery Form Plugin v2.47 or later (http://malsup.com/jquery/form/)
 *
 * Developed at: http://pragmas.org
 * by: Massimo maria Ghisalberti massimo.ghisalberti@gmail.com
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 */

//API_NAME = 'jhugApi Tumblr Client';
var API_VERSION =
		{
			'name' : 'jhugApi Tumblr Client',
			'major' : 0,
			'minor' : 6,
			'subminor' : 5,
			'patch' : 0,
			'betaFlag' : 'beta',
			versionString : function versionString() {
				return this['major'].toString() + '.' + this['minor'].toString() + '.'
						+ this['subminor'].toString() + '.' + this['patch'].toString()
						+ this['betaFlag'];
			}
		};

$.ajaxSetup( {
	async : true,
	cache : false
});

var TumblrApiSettings = {
	email : undefined,
	password : undefined,
	tags : undefined,
	signature : undefined,
	licence : undefined,
	defaultsTitle : undefined,
	hideMediaViewer : true,
	submitDefaults : true
};

var TumblrApi =
		{
			
			MULTIPARTHIDDENFORM_ID : "MULTIPARTHIDDENFORM_ID",
			REST_ADDRESS_WRITE : 'http://www.tumblr.com/api/write',
			REST_ADDRESS_REBLOG : 'http://www.tumblr.com/api/reblog',
			REST_ADDRESS_DELETE : 'http://www.tumblr.com/api/delete',
			
			RANGE_REQUEST_NUM : 8,
			
			REST_ADDRESS_ALL : 'http://www.tumblr.com/api/dashboard',
			REST_ADDRESS_LIKES : 'http://www.tumblr.com/api/likes',
			REST_ADDRESS_LIKE : 'http://www.tumblr.com/api/like',
			REST_ADDRESS_UNLIKE : 'http://www.tumblr.com/api/unlike',
			
			CURRENT_REST_ADDRESS : '',
			
			/* api write */
			type : 'regular',
			generator : API_VERSION['name'] + ' ' + API_VERSION.versionString(),
			date : undefined,
			private : undefined,
			tags : API_VERSION['name'] + ',',
			format : 'markdown',
			group : undefined,
			slug : undefined,
			state : 'published',
			send_to_twitter : 'no',
			
			/* api read */
			start : 0,
			num : this.RANGE_REQUEST_NUM,
			filter : undefined,
			type : undefined,
			likes : undefined,
			postsHTML : '',
			
			/* commons */
			latestResult : undefined,
			currentCallback : undefined,
			defaultsAttachs : undefined,
			defaultsTags : undefined,
			
			/* api write methods */
			generateData : function(type, data) {
				if (data) {
					data = $.param( {
						email : TumblrApiSettings.email,
						password : TumblrApiSettings.password,
						type : type,
						tags : this.tags,
						generator : this.generator
					}) + '&' + data;
				} else {
					data = {
						email : TumblrApiSettings.email,
						password : TumblrApiSettings.password,
						type : this.type,
						generator : this.generator,
						tags : this.tags,
						title : 'submission test title',
						body : 'submission test body'
					};
				}
				return data;
			},
			
			deletePost : function deletePost(post_id) {
				var rdata = this.generateData('regular', {});
				rdata += '&' + $.param( {
					'post-id' : post_id
				});
				if (EMULATOR) {
					netscape.security.PrivilegeManager
							.enablePrivilege("UniversalBrowserRead");
				}
				var __SELF = this;
				this.currentCallback = function() {
					alert("Successfully Deleted.");
				};
				$.post(this.REST_ADDRESS_DELETE, rdata, function(data, textStatus,
						XMLHttpRequest) {
					__SELF.latestResult = textStatus;
					if (__SELF.currentCallback) {
						__SELF.currentCallback();
						__SELF.currentCallback = undefined;
					}
				});
			},
			
			editPost : function editPost(post_id) {
				alert('not implemented yet.');
			},
			
			reblog : function reblog(type, post_id, reblog_key, comment) {
				var rdata = this.generateData(type, {});
				rdata += '&' + $.param( {
					'post-id' : post_id,
					'reblog-key' : reblog_key,
					'comment' : comment
				});
				if (EMULATOR) {
					netscape.security.PrivilegeManager
							.enablePrivilege("UniversalBrowserRead");
				}
				this.currentCallback = function() {
					alert("Successfully Reblogged.");
				};
				var __SELF = this;
				$.post(this.REST_ADDRESS_REBLOG, rdata, function(data, textStatus,
						XMLHttpRequest) {
					__SELF.latestResult = textStatus;
					if (__SELF.currentCallback) {
						__SELF.currentCallback();
						__SELF.currentCallback = undefined;
					}
				});
			},
			
			attach : function attach(type, form_id) {
				if (!TumblrApiSettings.submitDefaults)
					return false;
				var el = undefined;
				var form = $(form_id);
				el = form.find('[name="tags"]');
				if (el) {
					el.val(el.val() + ',' + this.defaultsTags);
					el = undefined;
				}
				if (type == 'regular') {
					el = form.find('[name="body"]');
				} else if (type == 'photo') {
					el = form.find('[name="caption"]');
				} else if (type == 'quote') {
					el = form.find('[name="source"]');
				} else if (type == 'link') {
					el = form.find('[name="description"]');
				} else if (type == 'conversation') {
					el = form.find('[name="conversation"]');
				} else if (type == 'audio') {
					el = form.find('[name="caption"]');
				} else if (type == 'video') {
					el = form.find('[name="caption"]');
				}
				if (el) {
					el.val(el.val() + '\n<br />' + this.defaultsAttachs);
					el = undefined;
				}
				// this.defaultsTags = '';
				// this.defaultsAttachs = '';
			},
			
			write : function write(type, data, writeType) {
				var form = $('#' + data);
				this.attach(type, form);
				if (writeType == 'multipart') {
					var inputFile = false;
					var children = form.find('input');
					children.each(function() {
						if ($(this).attr('type') == 'file') {
							inputFile = true;
						}
					});
					if (inputFile) {
						this.writeMultipart(type, data);
					} else {
						this.writeNormal(type, form.serialize());
					}
				} else {
					this.writeNormal(type, form.serialize());
				}
			},
			
			writeNormal : function writeNormal(type, data) {
				if (EMULATOR) {
					netscape.security.PrivilegeManager
							.enablePrivilege("UniversalBrowserRead");
				}
				var __SELF = this;
				$.post(this.REST_ADDRESS_WRITE, this.generateData(type, data),
						function(data, textStatus, XMLHttpRequest) {
							__SELF.latestResult = textStatus;
							if (__SELF.currentCallback) {
								__SELF.currentCallback();
								__SELF.currentCallback = undefined;
							}
						});
			},
			
			writeMultipart : function writeMultipart(type, dataForm_id) {
				if (EMULATOR) {
					netscape.security.PrivilegeManager
							.enablePrivilege("UniversalBrowserRead");
				}
				var form = undefined;
				var formExists = false;
				form = $(document.body).find('#' + this.MULTIPARTHIDDENFORM_ID);
				if (form.length > 0) {
					$(form).text('');
					formExists = true;
				} else {
					form =
							$('<form  action="'
									+ this.REST_ADDRESS_WRITE
									+ '" method="POST" name="MULTIPARTHIDDENFORM" id="'
									+ this.MULTIPARTHIDDENFORM_ID
									+ '" enctype="multipart/form-data" style="display:none;"></form>');
									//+ '" enctype="multipart/form-data"></form>');
					
				}
				
				form.append($('<textarea name="email">' + TumblrApiSettings.email
						+ '</textarea>'));
				form.append($('<textarea name="password">' + TumblrApiSettings.password
						+ '</textarea>'));
				form.append($('<textarea name="type">' + type + '</textarea>'));
				form.append($('<textarea name="tags">' + this.tags + '</textarea>'));
				form.append($('<textarea name="generator">' + this.generator
						+ '</textarea>'));
				var mainForm = $('#' + dataForm_id);
				var formElements = mainForm.children();
				formElements.each(function() {
					var cloned = $(this).clone();
					cloned.val($(this).val());
					//alert($(this).val())
					//alert(cloned.val());
					form.append(cloned);
				});
				if (!formExists) {
					$(document.body).append(form);
				}
				var __SELF = this;
				form.ajaxSubmit( {
					handleError : function(s, XMLHttpRequest, status, e) {
						// alert(XMLHttpRequest.status);
					},
					httpSuccess : function(XMLHttpRequest) {
						// alert(XMLHttpRequest.status);
					},
					complete : function(res, status) {
						// alert(res.status);
						if (res.status == 0) {
							__SELF.latestResult = status.toSource();
							if (__SELF.currentCallback) {
								__SELF.currentCallback();
								__SELF.currentCallback = undefined;
							}
						}
					},
					success : function(data, textStatus, XMLHttpRequest, $wel) {
						__SELF.latestResult = textStatus;
						if (__SELF.currentCallback) {
							__SELF.currentCallback();
							__SELF.currentCallback = undefined;
						}
					}
				});
			},
			/* api read methods */

			buildPostsHtml : function buildPostsHtml(data) {
				var postsHTML = '<ul class="tumblr-posts">';
				var title = '';
				var body = '';
				$(data)
						.find('post')
						.each(
								function() {
									if (this) {
										var tumblelog = $(this).attr('tumblelog');
										var id = $(this).attr('id');
										var type_of = $(this).attr('type');
										var note_count = $(this).attr('note-count');
										if (note_count == '')
											note_count = '0';
										var reblog_key = $(this).attr('reblog-key');
										var date_gmt = $(this).attr('date-gmt');
										var image = $(this).find('tumblelog').attr('avatar-url-40');
										var title = '';
										var body = '';
										
										if (type_of == 'regular') {
											title = $(this).find('regular-title').text();
											body = $(this).find('regular-body').text();
										} else if (type_of == 'photo') {
											title = $(this).find('photo-caption').text();
											var bodies =
													$(this).find('photo-url').each(
															function() {
																if ($(this).attr('max-width') == '250')
																	body =
																			'<img class="photo" src="'
																					+ $(this).text() + '" alt=""/>';
															});
										} else if (type_of == 'answer') {
											title = 'Question:';
											var question = $(this).find('question').text();
											var answer = $(this).find('answer').text();
											body = '<div class="question">' + question + '</div>';
											body += '<div class="answer">' + answer + '</div>';
										} else if (type_of == 'quote') {
											title = 'Quote:';
											var text = $(this).find('quote-text').text();
											var source = $(this).find('quote-source').text();
											body = '<div class="quote">' + text + '</div>';
											body += '<div class="source">' + source + '</div>';
										} else if (type_of == 'link') {
											title = 'Link:';
											var text = $(this).find('link-text').text();
											var url = $(this).find('link-url').text();
											var description = $(this).find('link-description').text();
											// body = '<div class="link">' + text + '</div>';
											body +=
													'<div class="link-url"><a href="' + url + '" title="'
															+ text + '">' + text + '</a></div>';
											body +=
													'<div class="link-description">' + description
															+ '</div>';
											// body +=
											// '<div class="link-url"><a href="' + url + '" title="'
											// + text + '">' + text + '</a></div>';
										} else if (type_of == 'conversation') {
											title = 'Conversation:';
											var ctitle = $(this).find('conversation-title').text();
											var ctext = $(this).find('conversation-text').text();
											body =
													'<div class="conversation-title">' + ctitle
															+ '</div>';
											body +=
													'<div class="conversation-text">' + ctext + '</div>';
											body += '<ul class="conversation-lines">';
											var convs = $(this).find('conversation');
											$(convs).find('line').each(
													function() {
														var label = $(this).attr('label');
														body +=
																'<li class="conversation-line">&nbsp;' + label
																		+ '&nbsp;' + $(this).text() + '</li>';
													});
											body += '</ul>';
										} else if (type_of == 'audio') {
											title = 'Audio:';
											var text = $(this).find('audio-caption').text();
											var source = $(this).find('audio-player').text();
											var url = $(this).find('download-url').text();
											body = '<div class="audio-title">' + text + '</div>';
											if (!TumblrApiSettings.hideMediaViewer) {
												body +=
														'<div class="audio-player">' + source + '</div>';
											}
											body +=
													'<div class="audio-url">download: <a href="' + url
															+ '" title="' + text + '">' + text + '</a></div>';
										} else if (type_of == 'video') {
											title = 'Video:';
											var text = $(this).find('video-caption').text();
											var source = $(this).find('video-player').text();
											var url = $(this).find('video-url').text();
											body = '<div class="video-title">' + text + '</div>';
											if (!TumblrApiSettings.hideMediaViewer) {
												body +=
														'<div class="video-player">' + source + '</div>';
											}
											body +=
													'<div class="video-url">download: <a href="' + url
															+ '" title="' + text + '">' + text + '</a></div>';
										}
										
										var tags = '';
										$(this).find('tag').each(function() {
											tags += '#' + $(this).text() + ' ';
										});
										if (title || body) {
											var post = '<li>';
											post +=
													'<img class="tumblr-post-image" src="' + image
															+ '" alt="">';
											post += '<dl id="' + id + '" class="tumblr-post '+ type_of +'">';
											post += '<dt class="title">' + title + '</dt>';
											post += '<dd class="infos">';
											
											post += '<div class="infos">';
											post +=
													'<span class="bold">Date:&nbsp;</span>' + date_gmt;
											post += '</div>';
											
											post += '<div class="infos commands">';
											post +=
													'<span class="like" onclick="TumblrApi.like(\'' + id
															+ '\', \'' + reblog_key + '\');">like</span>';
											post +=
													'<span class="like" onclick="TumblrApi.unlike(\''
															+ id + '\', \'' + reblog_key
															+ '\');">unlike</span>';
											post +=
													'<span class="like" onclick="TumblrApi.showReblogArea(this);">reblog</span>';
											post +=
													'<span class="like" onclick="TumblrApi.editPost(\''
															+ id + '\');">edit</span>';
											post +=
													'<span class="like" onclick="TumblrApi.deletePost(\''
															+ id + '\');">delete</span>';
											post += '</div>';
											
											post += '<div class="infos notes">';
											post +=
													'<span class="notes bold">notes:&nbsp;' + note_count
															+ '</span>';
											post += '</div>';
											
											post += '<div class="ui-widget Reblog Closed">';
											post += '<div class="Reblog-title">Reblog comment:</div>';
											post +=
													'<textarea class="ui-state-default Reblog small"></textarea>';
											post +=
													'<span class="like" onclick="TumblrApi.reblog('
															+ '\''
															+ type_of
															+ '\','
															+ '\''
															+ id
															+ '\','
															+ '\''
															+ reblog_key
															+ '\','
															+ 'TumblrApi.getReblogText(this));">reblog</span>';
											post += '</div>';
											
											post += '</dd>';
											
											post += '<dd class="body">' + body + '</dd>';
											post += '<dd class="tags">' + tags + '</dd>';
											post += '</dl><div class="close"></div><li>';
											postsHTML += post;
										}
									}
								});
				postsHTML += '</ul>';
				return postsHTML;
			},
			
			getReblogText : function getReblogText(el) {
				var p = $(el).parent();
				var area = p.find('textarea.Reblog');
				return '<div><br /><h2 style="font-weight:bold">Comment:</h2><p>'
						+ area.val() + '</p></div>';
			},
			
			showReblogArea : function showReblogArea(el) {
				var p = $(el).parent().parent();
				var area = p.find('div.Reblog');
				var vis = area.data('isVisible');
				if (vis && vis == true) {
					area.hide();
					area.data('isVisible', false);
				} else {
					area.show();
					area.data('isVisible', true);
				}
				return $(area);
			},
			
			getPosts : function getPosts(block, reset, likes) {
				this.CURRENT_REST_ADDRESS = this.REST_ADDRESS_ALL;
				if (likes && likes == 'likes') {
					this.CURRENT_REST_ADDRESS = this.REST_ADDRESS_LIKES;
				} else if (likes && likes == 'all') {
					this.CURRENT_REST_ADDRESS = this.REST_ADDRESS_ALL;
				}
				block = '#' + block;
				var __SELF = this;
				
				if (reset) {
					if (reset == 'forward') {
						__SELF.start = __SELF.start + __SELF.num;
					} else if (reset == 'backward') {
						__SELF.start = __SELF.start - __SELF.num;
					} else {
						__SELF.start = 0;
						__SELF.num = __SELF.RANGE_REQUEST_NUM;
					}
				}
				/* TODO: come vedere il limite massimo? */
				if (__SELF.start > -1) {
					if (__SELF.start < 0)
						__SELF.start = 0;
					if (EMULATOR) {
						netscape.security.PrivilegeManager
								.enablePrivilege("UniversalBrowserRead");
					}
					$.get(this.CURRENT_REST_ADDRESS, {
						email : TumblrApiSettings.email,
						password : TumblrApiSettings.password,
						start : this.start,
						num : this.num
					}, function(data, textStatus, XMLHttpRequest) {
						__SELF.latestResult = textStatus;
						__SELF.postsHTML = __SELF.buildPostsHtml(data);
						$(block).html(__SELF.postsHTML);
						$(block).show();
						if (__SELF.currentCallback) {
							__SELF.currentCallback();
							__SELF.currentCallback = undefined;
						}
					}, 'xml');
				} else
					__SELF.start = 0;
				if (__SELF.currentCallback) {
					__SELF.currentCallback();
					__SELF.currentCallback = undefined;
				}
			},
			
			like : function like(post_id, reblog_key) {
				var __SELF = this;
				this.currentCallback = function() {
					alert("Successfully Liked.");
				};
				if (!__SELF.currentCallback)
					this.currentCallback = function() {
						alert(this.latestResult);
					};
				if (EMULATOR) {
					netscape.security.PrivilegeManager
							.enablePrivilege("UniversalBrowserRead");
				}
				$.post(this.REST_ADDRESS_LIKE, {
					email : TumblrApiSettings.email,
					password : TumblrApiSettings.password,
					'post-id' : post_id,
					'reblog-key' : reblog_key
				}, function(data, textStatus, XMLHttpRequest) {
					__SELF.latestResult = textStatus;
					if (__SELF.currentCallback) {
						__SELF.currentCallback();
						__SELF.currentCallback = undefined;
					}
				});
			},
			
			unlike : function like(post_id, reblog_key) {
				var __SELF = this;
				this.currentCallback = function() {
					alert("Successfully Unliked.");
				};
				if (!__SELF.currentCallback)
					this.currentCallback = function() {
						alert(this.latestResult);
					};
				if (EMULATOR) {
					netscape.security.PrivilegeManager
							.enablePrivilege("UniversalBrowserRead");
				}
				$.post(this.REST_ADDRESS_UNLIKE, {
					email : TumblrApiSettings.email,
					password : TumblrApiSettings.password,
					'post-id' : post_id,
					'reblog-key' : reblog_key
				}, function(data, textStatus, XMLHttpRequest) {
					__SELF.latestResult = textStatus;
					if (__SELF.currentCallback) {
						__SELF.currentCallback();
						__SELF.currentCallback = undefined;
					}
				});
			}
		};
